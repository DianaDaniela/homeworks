//Check entire form
function checkform() {
	validateFirstName();
	validateLastName();
	validateUserName();
	validateAbout();
	validatePassword();
	validateConfirmPassword();
	validateEmail();
	validateCheckGender();
	validateCheckSubscribe();
	validateSubmit();
}

//validate first name
function validateFirstName() {
	var firstName = document.getElementById("fname").value;
	if(firstName.length == 0 || firstName.length < 2 || firstName.length > 40 ) {
		document.getElementById("errorFname").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
}

//validate last name
function validateLastName() {
	var lastName = document.getElementById("lname").value;
	if(lastName.length == 0 || lastName.length < 2 || lastName.length > 40) {
		document.getElementById("errorLname").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
}

//validate e-mail
function validateEmail() {
	var email = document.getElementById("email").value;
	if(email.length == 0 || email.length < 2 || email.length > 40) {
		document.getElementById("errorEmail").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}	
}

//validate username
function validateUserName() {
	var userName = document.getElementById("user").value;
	if(userName.length == 0 || userName.length < 2 || userName.length > 40) {
		document.getElementById("errorUser").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
}

//validate password
function validatePassword() {
	var passwordPass = document.getElementById("password").value;
	if(passwordPass.length == 0 || passwordPass.length < 2 || passwordPass.length > 40) {
		document.getElementById("errorPass").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
}	

//validate confirm password
function validateConfirmPassword() {
	var confirmPassword = document.getElementById("cpass").value;
	if(confirmPassword.length == 0 || confirmPassword.length < 2 || confirmPassword.length > 40) {
		document.getElementById("confPass").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
			
}	

//validate gender
function validateCheckGender() {
	if (document.getElementById("male").checked == false && document.getElementById("female").checked == false){
		document.getElementById("checkgender").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}	
}

//validate about
function validateAbout() {
	var about = document.getElementById("text").value;
	if(about.length == 0 || about.length < 2 || about.length > 40){
		document.getElementById("errorAbout").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
}

//validate subscribe
function validateCheckSubscribe() {
	if (document.getElementById("check").checked == false){
		document.getElementById("checksubscribe").style.visibility = "visible";
		return false;
	}
	else {
		return true;
	}
}

//validate submit
function validateSubmit(){
if (validateFirstName() == true && 
	validateLastName() == true && 
	validateUserName() == true && 
	validateAbout() == true && 
	validatePassword() == true && 
	validateConfirmPassword() == true && 
	validateEmail() == true && 
	validateCheckGender() == true && 
	validateCheckSubscribe() == true){
		document.getElementById("submitform").style.visibility = "visible";
	}
}
