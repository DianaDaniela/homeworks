package ro.siit.curs4.homework3;

public class Library {

    public static void main(String[] args) {

        Author a = new Author("Ion Creanga", "ioncreanga@yahoo.com");
        Book b = new Book("Capra cu trei iezi", 1875, a, 30);

        System.out.println("Book " +  b.getName() + " (" + b.getPrice() + " RON), by " +  a.getName() + ", published in " + b.getYear());
    }
}
