package ro.siit.curs4.homework3;

public class Author {

    public String name, email;

    public Author (String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String geEmail() {
        return email;
    }
}
