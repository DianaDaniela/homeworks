package ro.siit.curs6;

public enum Room {
    OFFICE_SPACE("Office space"),
    KITCHEN("Kitchen"),
    TOILET("Toilet"),
    CONFERENCE_ROOM("Conference room");

    private String type;

    Room(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
