import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.siit.curs7.Calculator;

public class CalculatorTest {
    static Calculator c;

    @Before
    public void beforeTest() {
        c = new Calculator();
    }


//testing Sum
    @Test
    public void testSum01() {
//        System.out.println( c.compute(2,7,"+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void testSum04() {
//        System.out.println( c.compute(2,7,"+"));
        Assert.assertNotEquals(8, c.compute(2, 7, "+"));
    }

    @Test
    public void testSum02() {
        Assert.assertEquals(-13453, c.compute(2164, -15617, "+"), 0);
    }

    @Test
    public void testSum03() {
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test
    public void testSum05() {
        Assert.assertNotNull(c.compute(1000, 0, "+"));
    }

    @Test
    public void testSum08() {
        Assert.assertEquals(20.03, c.compute(10.01, 10.02, "+"), 0);
    }

    @Test
    public void testSum09() {
        Assert.assertEquals(0, c.compute(10.01, -10.01, "+"), 0);
    }

    @Test
    public void testSum010() {
        Assert.assertEquals(-20.02, c.compute(-10.01, -10.01, "+"), 0);
    }

    @Test
    public void testSum06(){
    Assertions.assertThat(c.compute(1000, 0, "+")).isNotNull();
    }

    @Test
    public void testSum07(){
        Assertions.assertThat(c.compute(2,2,"+")).isEqualTo(4.0);
    }


    //testing Diff
    @Test
    public void testDiff01(){
        Assert.assertEquals(900,c.compute(1000,100,"-"),0);
    }

    @Test
    public void testDiff05(){
        Assert.assertEquals(-8,c.compute(-3,5,"-"),0);
    }

    @Test
    public void testDiff08(){
        Assert.assertEquals(-2,c.compute(-7,-5,"-"),0);
    }

    @Test
    public void testDiff02(){
        Assert.assertNotEquals(90,c.compute(1000,100,"-"));
    }

    @Test
    public void testDiff03(){
        Assert.assertNotNull(c.compute(1000,100,"-"));
    }

    @Test
    public void testDiff006(){
        Assert.assertEquals(900.10,c.compute(1000.20,100.10,"-"),0);
    }

    @Test
    public void testDiff007(){
        Assert.assertEquals(-900.10,c.compute(-1000.20,-100.10,"-"),0);
    }

    @Test
    public void testDiff04(){
        Assertions.assertThat(c.compute(1000,100,"-")).isEqualTo(900.0);
    }


    //testing Prod
    @Test
    public void testProd01(){
        Assert.assertEquals(121,c.compute(11,11,"*"),0);
    }

    @Test
    public void testProd05(){
        Assert.assertEquals(-121,c.compute(11,-11,"*"),0);
    }

    @Test
    public void testProd02(){
        Assert.assertNotNull(c.compute(0,0,"*"));
    }

    @Test
    public void testProd03(){
        Assert.assertNotEquals(-121,c.compute(11,11,"*"),0);
    }

    @Test
    public void testProd06(){
        Assert.assertEquals(123.21,c.compute(11.10,11.10,"*"),0);
    }

    @Test
    public void testProd07(){
        Assert.assertEquals(-123.21,c.compute(11.10,-11.10,"*"),0);
    }

    @Test
    public void testProd04(){
        Assertions.assertThat(c.compute(11,11,"*")).isEqualTo(121.0);
    }

    @Test
    public void testProd08(){
        Assert.assertEquals(0,c.compute(11,0,"*"),0);
    }

    @Test
    public void testProd10(){
        Assert.assertEquals(0,c.compute(-11,0,"*"),0);
    }

    @Test
    public void testProd09(){
        Assert.assertEquals(0,c.compute(11.10,0,"*"),0);
    }


    //testing unSupported
    @Test(expected = IllegalArgumentException.class)
    public void unsupportedTest01(){
        Assert.assertEquals(121,c.compute(11,11,"x"),0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unsupportedTest02(){
        Assert.assertEquals(100,c.compute(10,10,"&"),0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unsupportedTest03(){
        Assert.assertNotEquals(-121,c.compute(11,11,"x"),0);
    }


    //testing div
    @Test
    public void testDiv01(){
        Assert.assertEquals(2,c.compute(10,5,"/"),0);
    }

    @Test
    public void testDiv06(){
        Assert.assertEquals(-2,c.compute(10,-5,"/"),0);
    }

    @Test
    public void testDiv02(){
        Assert.assertEquals(3.33,c.compute(10,3,"/"),0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDiv04(){
        Assert.assertNotNull(c.compute(0,0,"/"));
    }

    // varianta corecta pt tratarea exceptiilor
    @Test(expected = IllegalArgumentException.class)
    public void testDiv03(){
        Assert.assertEquals(0,c.compute(10,0,"/"),0);
    }

    // this is just to catch the exception and the test will be passed either way, please do not use these example
//    @Test
//    public void testDiv04(){
//        try {
//            Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
//        } catch (IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
//
//    }
    @Test
    public void testDiv05(){
        Assert.assertNotEquals(-2,c.compute(10,5,"/"),0);
    }

    @Test
    public void testDiv07(){
        Assert.assertEquals(1.9980,c.compute(10.01,5.01,"/"),0.01);
    }

    @Test
    public void testDiv08(){
        Assert.assertEquals(-1.99,c.compute(10.01,-5.01,"/"),0.01);
    }


    //testing SQRT
    @Test
    public void testSqrt01(){
        Assert.assertEquals(1.4142,c.compute(2,0,"SQRT"),0.001);
    }

    @Test
    public void testSqrt02(){
        Assert.assertNotEquals(1.5,c.compute(2,0,"SQRT"));
    }

    @Test
    public void testSqrt04(){
        Assert.assertNotEquals(-1.4142,c.compute(2,0,"SQRT"));
    }

    @Test
    public void testSqrt03(){
        Assert.assertNotNull(c.compute(2,0,"SQRT"));
    }

}
