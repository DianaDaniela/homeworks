package tests;

import Utils.OtherUtils;
import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Cookies extends BaseTest {
    @Test
     public void CookieHomework() {
        driver.get(hostname + "/stubs/cookie.html");
        OtherUtils.printCookies(test, driver);
        Cookie homeCookie = new Cookie("myHomeCookie", "123val");
        driver.manage().addCookie(homeCookie);
        Cookie driverCookie = driver.manage().getCookieNamed("myHomeCookie");
        OtherUtils.printCookie(test, homeCookie);
        Assert.assertEquals(driverCookie.getValue(), "123val");
        OtherUtils.printCookies(test, driver);
        driver.manage().deleteCookie(homeCookie);
        WebElement removeCookieButton = driver.findElement(By.id("delete-cookie"));
        removeCookieButton.click();
        OtherUtils.printCookies(test, driver);
        String removeCookieValue = driver.findElement(By.id("cookie-value")).getText();
        Assert.assertEquals(removeCookieValue, "");
    }
}
