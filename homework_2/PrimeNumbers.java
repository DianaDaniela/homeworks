package ro.siit.curs3.homework2;

public class PrimeNumbers {

    //Display all the prime numbers lower than 1 000 000
    public static void main(String[] args) {

        for(int nr = 0; nr < 1000000; nr++) {
            boolean isPrime = true;
            for (int x = 2; x <= nr/2; x++) {
                if (nr % x == 0) {
                    isPrime = false;
                }
            }
            if (isPrime == true) {
                System.out.println(nr);
            }
        }
    }
}
