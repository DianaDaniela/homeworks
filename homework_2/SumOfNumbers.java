package ro.siit.curs3.homework2;

public class SumOfNumbers {

    // Calculate the sum of the first 100 numbers higher then 0
    public static void main(String[] args) {

        int sum = 0;
        for(int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        System.out.println("Sum is: " + sum);
    }
}
