package ro.siit.curs3.homework2;

public class LeapYear {

    //Display the numbers of days in month February from a year between 1900-2016 that is read from argument
    public static void main(String[] args) {

        int y = Integer.parseInt(args[0]);

        if (y >= 1900 && y <= 2016) {
            if (y % 4 == 0 && y % 100 != 0 || y % 400 == 0) {
                System.out.println("In " + y + " February has 29 days!");
            } else {
                System.out.println("In " + y + " February has 28 days!");
            }
        }
        else{
            System.out.println("Year is not between 1900 and 2016!");
        }
    }
}
